# Go Collection Generator

This generator finds all type declarations in the .go files in a given directory and creates collection types for them.

# Usage

```
//go:generate go run gen -i <input directory path> -o <output file path>
```
