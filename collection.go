package main

// TODO(kjh): allow specify type filter

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"text/template"
)

var iptdir string
var outfil string
var errSkip = errors.New("skip")

func init() {
	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	iptdir = *flag.String("i", cwd, fmt.Sprintf("Input file/directory. Default: %s", cwd))
	outfil = *flag.String("o", cwd+"/collection.go", fmt.Sprintf("Output file. Default: %s", cwd+"/collection.go"))

	flag.Parse()
}

func main() {
	var pkg string
	var types []string
	var inRoot = false
	var errBreakWalk error

	log.Printf("[INPUT] %s\n", iptdir)
	log.Printf("[OUTPUT] %s\n", outfil)

	filepath.Walk(iptdir, func(path string, info os.FileInfo, err error) error {
		// Get a valid path
		if err != nil {
			log.Printf("[ERROR] getting valid path: %s\n", err)
			return nil
		}

		// If we're not yet in the "root", then we need to figure out if this
		// is a dir request or a file request. If it's a file request, then we
		// already have the file, so carry on. Otherwise, if this is a dir
		// request, we need to walk into it.
		if !inRoot {
			inRoot = true
			if info.IsDir() {
				return nil
			}
			errBreakWalk = errors.New("Break Walk")
		}

		if info.IsDir() {
			return filepath.SkipDir
		}

		// Make sure we're working with a go file
		if !strings.HasSuffix(info.Name(), ".go") {
			return errBreakWalk
		}

		// No test files
		if strings.HasSuffix(info.Name(), "_test.go") {
			return errBreakWalk
		}

		// Open and parse the input file
		ifile, err := os.Open(info.Name())
		if err != nil {
			log.Printf("[ERROR] opening file: %s; %s\n", info.Name(), err)
			return errBreakWalk
		}
		defer ifile.Close()

		// Get the package and types
		p, t, err := GetPackageAndTypes(ifile)
		if err != nil {
			if err != errSkip {
				log.Printf("[ERROR] getting packages and types: %s\n", err)
			}
			return errBreakWalk
		}

		pkg = p
		types = append(types, t...)

		if err := ifile.Close(); err != nil {
			log.Fatalf("[FATAL] closing file: %s; %s\n", info.Name(), err)
		}

		return errBreakWalk
	})

	log.Printf("[INFO] types: %v\n", types)

	// Create and write to the output file
	ofile, err := os.Create(outfil)
	if err != nil {
		log.Fatalf("[FATAL] creating output file: %s\n", err)
	}
	defer ofile.Close()

	if err := Render(ofile, pkg, types); err != nil {
		log.Fatalf("[FATAL] rendering: %s\n", err)
	}

	if err := ofile.Close(); err != nil {
		log.Fatalf("[FATAL] closing output file: %s\n", err)
	}

	// run go imports
	cmd := exec.Command("goimports", "-w", outfil)
	if err := cmd.Run(); err != nil {
		log.Fatalf("[FATAL] running goimports: %s\n", err)
	}

	// Done!
	log.Println("~~~ SUCCESS ~~~")
}

func GetPackageAndTypes(r io.Reader) (string, []string, error) {
	var pkg string
	types := []string{}

	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		line := scanner.Text()
		words := strings.Fields(line)

		if len(words) < 2 {
			continue
		}

		if len(words) >= 2 && words[0] == "package" {
			pkg = words[1]
			continue
		}

		if len(words) < 3 {
			continue
		}

		// Ignore build ignores and generated code
		if words[0] == "//" && words[1] == "+build" && words[2] == "ignore" {
			return "", make([]string, 0), errSkip
		}
		if words[0] == "//" && words[1] == "Code" && words[2] == "generated" {
			return "", make([]string, 0), errSkip
		}

		// Ignore interfaces and generic interface types
		if words[0] == "type" && words[2] != "interface{}" && words[2] != "interface" {
			types = append(types, words[1])
		}
	}

	return pkg, types, scanner.Err()
}

func Render(w io.Writer, pkg string, types []string) error {
	tpl, err := template.New("template").Parse(TEMPLATE)
	if err != nil {
		return err
	}

	// Add os.Stderr to see output in console too
	writers := []io.Writer{w}

	for _, wr := range writers {
		err := tpl.Execute(wr, &TemplateData{
			Package:    pkg,
			Types:      types,
			BasicTypes: []BasicType{"bool", "int", "int8", "int16", "int32", "int64", "uint", "uint8", "uint16", "uint32", "uint64", "uintptr", "float32", "float64", "complex64", "complex128", "string"},
		})
		if err != nil {
			return err
		}
	}

	return nil
}

type BasicType string

func (t BasicType) Title() string {
	return strings.Title(string(t))
}

type TemplateData struct {
	Package    string
	Types      []string
	BasicTypes []BasicType
}

const TEMPLATE = `// Code generated  DO NOT EDIT.
package {{.Package}}
{{ $types := .Types }}
{{ $basicTypes := .BasicTypes }}
{{range $type := $types}}
// {{$type}}Collection provides various convenient slice traversal methods.
type {{$type}}Collection []{{$type}}

// ForEach calls the callback for each of the elements.
func (vs {{$type}}Collection) ForEach(f func(*{{$type}}) error) {
	for _, v := range vs {
		if err := f(&v); err != nil {
			return
		}
	}
}

// Filter returns a new slice containing all {{$type}}s in the
// slice that satisfy the predicate 'f'.
func (vs {{$type}}Collection) Filter(f func({{$type}}) bool) {{$type}}Collection {
    vsf := make({{$type}}Collection, 0)
    for _, v := range vs {
        if f(v) {
            vsf = append(vsf, v)
        }
    }
    return vsf
}
{{range $basicType := $basicTypes}}
// Map returns a new slice of []{{$basicType}} containing the results of applying
// the function 'f' to each {{$type}} in the original slice.
func (vs {{$type}}Collection) Map{{$basicType.Title}}s(f func({{$type}}) {{$basicType}}) []{{$basicType}} {
	vsm := make([]{{$basicType}}, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}
{{end}}
{{range $otherType := $types}}
// Map returns a new {{$otherType}}Collection containing the results of applying
// the function 'f' to each {{$type}} in the original slice.
func (vs {{$type}}Collection) Map{{$otherType}}Collection(f func({{$type}}) {{$otherType}}) {{$otherType}}Collection {
	vsm := make({{$otherType}}Collection, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}
{{end}}
{{end}}`
